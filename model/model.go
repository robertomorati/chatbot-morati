package model

import (
	"encoding/json"

	"github.com/gofrs/uuid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type Rating float64
type ChatType string
type StatusOrder string

const (
	UserMessage ChatType = "user_message"
	BotResponse ChatType = "bot_response"
)

const (
	OrderPending   StatusOrder = "pending"
	OrderConfirmed StatusOrder = "confirmed"
	OrderDelivered StatusOrder = "delivered"
)

type User struct {
	ID       string `gorm:"type:varchar(36);primary_key"`
	Name     string
	Username string `gorm:"type:varchar(20);unique_index"`
	Email    string `gorm:"type:varchar(100);unique_index"`
	Password string `gorm:"type:varchar(100)"`
	gorm.Model
}

type Product struct {
	gorm.Model
	Name            string
	Category        string
	Characteristics json.RawMessage `gorm:"type:json"`
}

// BeforeCreate https://gorm.io/docs/create.html#Create-Hooks
func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	if u.ID == "" {
		u.ID = uuid.Must(uuid.NewV4()).String()
	}

	if pw, err := bcrypt.GenerateFromPassword([]byte(u.Password), 0); err == nil {
		tx.Statement.SetColumn("Password", pw)
	}
	return nil
}

type Chat struct {
	SessionID string `gorm:"type:varchar(36)"`
	UserID    string
	User      User
	Type      ChatType        `gorm:"type:varchar(50);"`
	Message   string          `gorm:"type:text;"`
	Metadata  json.RawMessage `gorm:"type:json"`
	gorm.Model
}

func (c *Chat) BeforeCreate(tx *gorm.DB) (err error) {
	if c.SessionID == "" {
		c.SessionID = uuid.Must(uuid.NewV4()).String()
	}
	return nil
}

type Review struct {
	gorm.Model
	ProductID int64
	Product   Product
	UserID    string
	User      User
	Rating    Rating `gorm:"type:integer;check:rating >= 1 AND rating <= 5"`
	Comment   string
}

type Order struct {
	gorm.Model
	UserID    string
	User      User
	ProductID int64
	Product   Product
	Status    StatusOrder
}
