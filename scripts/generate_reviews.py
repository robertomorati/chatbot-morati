import os
import pandas as pd

# script to process reviews from amazon dataset to get sentiment labels and generate a new csv file
# with this new csv file we generate a news jsonl and train a sentiment analysis model with open AI

def get_sentiment_label(rating):
    if rating in [4, 5]:
        return "positive"
    elif rating == 3:
        return "neutral"
    elif rating in [1, 2]:
        return "negative"
    return None

def process_csv_reviews(file_path: str):
    reviews = []

    df = pd.read_csv(file_path)

    for index, row in df.iterrows():
        if pd.isnull(row[14]):
            continue
        review = "I'd give it a %s. %s" % (row[14], row[16])
        rating = int(row[14])
        review = {
            'sentiment': get_sentiment_label(rating),
            'review': review
        }
        if review['sentiment'] is not None:
            reviews.append(review)

    return reviews

def save_reviews_to_csv(reviews, file_path: str):
    df = pd.DataFrame(reviews)
    df.to_csv(file_path, index=False)         


if __name__ == '__main__':
    
    input_file_path = os.path.join(os.path.dirname(__file__), '../models/1429_1.csv')
    output_file_path = os.path.join(os.path.dirname(__file__), '../models/review_given.csv')
    
    reviews = process_csv_reviews(input_file_path)
    save_reviews_to_csv(reviews, output_file_path)
