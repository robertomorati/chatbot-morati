
# Chatbot
#### Chatbot for product reviews


## 1. Clone the project

    git clone https://gitlab.com/robertomorati/chatbot-morati.git

## 2.  Running the Project

To run this project, it is necessary to fine-tune the `models/training_model_14052024.jsonl` model. Additionally, export the `OPENAI_API_KEY` with your key and `OPENAI_MODEL` with your model to run the `ProcessMessage` successfully.

```shell
export OPENAI_API_KEY=your_api_key
export OPENAI_MODEL=your_model
```


Besides that, it is needed to have MySQL installed and update the `settings/config.json` file. Also, export `DB_PASSWORD` with your test password.
```
{
  "database": {
    "host": "localhost",
    "port": 3306,
    "user": "root",
    "dbname": "database_name"
  }
}
```
```shell
export DB_PASSWORD=your_test_password
```

To send a signal for order delivered, use the following command:

```shell
curl -X POST http://localhost:8080/order-delivered -H "Content-Type: application/json" -d '{"order_id": "order_id", "user_id": "user_id"}'
```

## 3.  Running the Project

To run the tests in the project, use the command:

```
go test ./...
```

## 3.  [Backend] [Abstract] Chatbot for a product review

For more details, refer to the document: [Chatbot for a product review](https://docs.google.com/document/d/1yO4WLDGHKAUDkddkMRPxnON2o-uNSzINc45ve1e-GhA/edit)