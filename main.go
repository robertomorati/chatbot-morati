package main

import (
	"log"
	chatbot "morati-chatbot/handler"
	"morati-chatbot/repository"
	"net/http"
	"sync"
)

func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != http.MethodGet {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "home.html")
}

// TODO encapsulate logic from main
// TODO remove InitiDB and MigrateDB from main
func main() {
	db := repository.InitDB()
	repository.CloseDB(db)
	repository.MigrateDB(true)

	session := &sync.Map{}
	params := chatbot.NewParams(session)

	http.HandleFunc("/", serveHome)
	http.HandleFunc("/ws", params.ChatbotHandler)
	http.HandleFunc("/order-delivered", params.HandleOrderDelivered)
	err := http.ListenAndServe(":8080", nil)

	if err != nil {
		println("Error starting server", err)
	}

}
