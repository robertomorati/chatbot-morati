package mocks

import (
	"morati-chatbot/model"

	"gorm.io/gorm"
)

func CreateMockUsers(db *gorm.DB) error {
	users := []model.User{
		{ID: "b1439d08-1a7b-4e1d-8487-c28b1209b170", Name: "Roberto Morati", Username: "robertomorati", Email: "robertomorati@gmail.com", Password: "password"},
		{ID: "b1439d08-1a7b-4e1d-8487-c28b1209b171", Name: "John Doe", Username: "johndoe", Email: "johndoe@email.com", Password: "password"},
	}
	for _, user := range users {
		if err := db.Create(&user).Error; err != nil {
			return err
		}
	}
	return nil
}
