package constants

type EventType string

const (
	ProductAsked           EventType = "product_asked"          // user asks for a product details
	ProductOrder           EventType = "product_order"          // user orders a product if has in our db
	ProductOrdered         EventType = "product_ordered"        // user orders a product if not in our db
	ProductOrderedDeclined EventType = "product_order_declined" // user declines the order
	DeliveryConfirmed      EventType = "delivery_confirmed"     // user confirms delivery
	ReviewGiven            EventType = "review_given"           // user gives review
	ReviewDeclined         EventType = "review_declined"        // user declines to give review
	ProductRecommended     EventType = "product_recommended"    // user ask for a recommendation
	ProductOutdated        EventType = "product_outdated"       // user asks for an outdated product
)
