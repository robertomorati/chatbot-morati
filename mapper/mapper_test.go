package mapper

import (
	"io"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestToOpenAIRequest(t *testing.T) {

	tests := map[string]struct {
		model     string
		message   string
		maxTokens int
		expected  OpenAIRequest
	}{

		"open_ai_request": {
			model:     "model",
			message:   "message",
			maxTokens: 10,
			expected: OpenAIRequest{
				Model: "model",
				Messages: []ChatMessage{
					{
						Role:    "user",
						Content: "message",
					},
				},
				MaxTokens: 10,
			},
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			assert.Equal(t, tc.expected, ToOpenAIRequest(tc.model, tc.message, tc.maxTokens))
		})
	}
}

func TestOpenAIResponseToMessage(t *testing.T) {

	tests := map[string]struct {
		body     io.ReadCloser
		expected OpenAIResponse
	}{
		"open_ai_response": {
			body: io.NopCloser(strings.NewReader(`{
				"choices": [{
					"message": {
						"content": "Test response"
					}
				}]
			}`)),
			expected: OpenAIResponse{
				Choices: []struct {
					Message struct {
						Content string `json:"content"`
					} `json:"message"`
				}{{Message: struct {
					Content string `json:"content"`
				}{Content: "Test response"}}},
			},
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			r, _ := OpenAIResponseToMessage(tc.body)
			assert.Equal(t, tc.expected, r)
		})
	}
}
