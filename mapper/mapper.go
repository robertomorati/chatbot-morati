package mapper

import (
	"encoding/json"
	"io"
)

type ChatMessage struct {
	Role    string `json:"role"`
	Content string `json:"content"`
}

type OpenAIRequest struct {
	Model     string        `json:"model"`
	Messages  []ChatMessage `json:"messages"`
	MaxTokens int           `json:"max_tokens"`
}

type OrderDeliveredRequest struct {
	OrderID string `json:"order_id"`
	UserID  string `json:"user_id"`
}

type OpenAIResponse struct {
	Choices []struct {
		Message struct {
			Content string `json:"content"`
		} `json:"message"`
	} `json:"choices"`
}

func ToOpenAIRequest(model string, message string, maxTokens int) OpenAIRequest {
	return OpenAIRequest{
		Model: model,
		Messages: []ChatMessage{
			{
				Role:    "user",
				Content: message,
			},
		},
		MaxTokens: maxTokens,
	}
}

func OpenAIResponseToMessage(body io.ReadCloser) (OpenAIResponse, error) {
	var response OpenAIResponse
	if err := json.NewDecoder(body).Decode(&response); err != nil {
		return response, err
	}

	return response, nil
}
