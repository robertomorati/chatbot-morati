package services

import (
	"bytes"
	"encoding/json"
	"fmt"
	"morati-chatbot/mapper"
	"morati-chatbot/util"
	"net/http"
)

func GetResponse(message string, apiKey string, model string) (string, error) {

	// TODO move to constants/config
	url := "https://api.openai.com/v1/chat/completions"

	r := mapper.ToOpenAIRequest(model, message, 150)

	reqBody, err := json.Marshal(r)
	if err != nil {
		return "", err
	}

	// TODO move to utils/http
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(reqBody))
	if err != nil {
		return "", err
	}

	util.SetCommonHeaders(req, apiKey)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", fmt.Errorf("failed to send request: %w", err)
	}

	// closing the response body
	defer resp.Body.Close()

	// TODO move to mapper
	response, err := mapper.OpenAIResponseToMessage(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed to parse response: %w", err)
	}

	if len(response.Choices) > 0 {
		return response.Choices[0].Message.Content, nil
	}

	return "", fmt.Errorf("no response found from assistant")
}
