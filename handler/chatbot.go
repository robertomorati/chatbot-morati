package chatbot

import (
	"encoding/json"
	"morati-chatbot/controller"
	"morati-chatbot/mapper"
	"net/http"
	"strconv"
	"sync"

	"github.com/gofrs/uuid"
	"github.com/gorilla/websocket"

	"gorm.io/gorm"
)

type Params struct {
	DB             *gorm.DB
	Conn           *websocket.Conn //TODO (robertomorati) change to a map of connections with user UUID
	ChatController *controller.ChatController
	Session        *sync.Map
	Mutex          sync.Mutex
	UserID         uuid.UUID
}

func NewParams(session *sync.Map) *Params {
	workflowService := controller.NewWorkflowService(session)
	chatController := controller.NewChatController(workflowService)
	return &Params{
		Session:        session,
		ChatController: chatController,
	}
}

// TODO remove from here
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true // keep false for testing
	},
}

func (p *Params) ChatbotHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		http.Error(w, "Could not open websocket connection", http.StatusBadRequest)
		return
	}
	defer conn.Close()
	p.SetWebSocketConn(conn)

	// TODO for tessting purposes remove later
	userId, err := uuid.FromString("b1439d08-1a7b-4e1d-8487-c28b1209b170")
	if err != nil {
		// TODO handle the error
		return
	}
	p.UserID = userId
	p.Session.Store("user", userId)

	for {
		messageType, msg, err := conn.ReadMessage()
		if err != nil {
			break
		}

		m := p.ChatController.ProcessMessage(userId, string(msg))
		if err := conn.WriteMessage(messageType, []byte(m)); err != nil {
			break
		}
	}
	p.CleanWebSocketConn()
	p.ChatController.Clean()
}

// TODO encapsulate logic and added validation for user and order IDs
func (p *Params) HandleOrderDelivered(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	request := mapper.OrderDeliveredRequest{}

	userUUID, _, shouldReturn := p.ValidateOrderDelivered(r, request, w)
	if shouldReturn {
		return
	}

	m := p.ChatController.ProcessMessage(userUUID, "I received the order at my home, confirm delivery")

	conn := p.GetWebSocketConn()

	if err := conn.WriteMessage(websocket.TextMessage, []byte(m)); err != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("The order delivered signal received"))
}

func (*Params) ValidateOrderDelivered(r *http.Request, request mapper.OrderDeliveredRequest, w http.ResponseWriter) (uuid.UUID, int64, bool) {
	var userUUID uuid.UUID
	var orderID int64
	var err error
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return userUUID, -1, true
	}

	if request.OrderID == "" || request.UserID == "" {
		http.Error(w, "Order ID and User ID are required", http.StatusBadRequest)
		return userUUID, -1, true
	}

	userUUID, err = uuid.FromString(request.UserID)
	if err != nil {
		http.Error(w, "Invalid user ID", http.StatusBadRequest)
		return userUUID, -1, true
	}

	orderID, err = strconv.ParseInt(request.OrderID, 10, 64)
	if err != nil {
		http.Error(w, "Invalid order number", http.StatusBadRequest)
		return userUUID, -1, true
	}

	return userUUID, orderID, false
}

func (p *Params) GetWebSocketConn() *websocket.Conn {
	p.Mutex.Lock()
	conn := p.Conn
	p.Mutex.Unlock()
	return conn
}

func (p *Params) SetWebSocketConn(conn *websocket.Conn) {
	p.Mutex.Lock()
	p.Conn = conn
	p.Mutex.Unlock()
}

func (p *Params) CleanWebSocketConn() {
	p.Mutex.Lock()
	p.Conn = nil
	p.Mutex.Unlock()
}
