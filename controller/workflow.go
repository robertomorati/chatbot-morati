package controller

import (
	"morati-chatbot/repository"
	"regexp"
	"strconv"
	"sync"
)

type WorkflowController struct {
	Repository *repository.Repository
	Session    *sync.Map
}

func NewWorkflowService(s *sync.Map) *WorkflowController {
	r := repository.NewRepository()
	return &WorkflowController{
		Repository: r,
		Session:    s,
	}
}

func (s *WorkflowController) Clean() {
	s.Repository.Clean()
}

func (s *WorkflowController) CreateOrder(userId string, productName string) int64 {
	product, _ := s.Repository.GetProductByName(productName)
	order, _ := s.Repository.CreateOrder(userId, int64(product.ID))
	s.Session.Store("order", int64(order.ID))
	return int64(order.ID)
}

func (s *WorkflowController) RegisterChatUser(userId string, chatType string, message string) {
	_, _ = s.Repository.CreateChat(userId, chatType, message)
}
func (s *WorkflowController) CompleteReviewProcess(userId string, productName, message string) {
	rating := s.extractRating(message)
	_, err := s.Repository.CreateReview(userId, productName, rating, message)
	if err != nil {
		println("Error creating review", err)
	}
}

func (s *WorkflowController) ProcessOrderDelived(userId string) {
	orderId, ok := s.Session.LoadAndDelete("order")
	if !ok {
		return
	}
	err := s.Repository.UpdateOrderStatusToDelivered(orderId.(int64))
	if err != nil {
		println("Error updating order status", err)
	}
}
func (s *WorkflowController) SendProductRecommendation(userId string) {
	// TODO impplement treatment for response to product recommendation
}

func (s *WorkflowController) StartReviewProcess(userId string) {
	// TODO implement the review process
}

func (s *WorkflowController) HandleUserResponse(userId string, response string) {
	// TODO implement treatment for user response
}

func (s *WorkflowController) extractRating(message string) float64 {
	re := regexp.MustCompile(`\d+(\.\d+)?`)
	matches := re.FindStringSubmatch(message)
	if len(matches) == 0 {
		return 3.0
	}
	rating, _ := strconv.ParseFloat(matches[0], 64)
	return rating
}
