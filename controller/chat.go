package controller

import (
	"encoding/json"
	"fmt"
	"morati-chatbot/constants"
	"morati-chatbot/services"
	"os"

	"github.com/gofrs/uuid"
)

type ChatController struct {
	workflow *WorkflowController
}

func NewChatController(workflow *WorkflowController) *ChatController {
	return &ChatController{
		workflow: workflow}
}

func (s *ChatController) Clean() {
	s.workflow.Clean()
}

func (c *ChatController) ProcessMessage(userId uuid.UUID, message string) string {

	key := os.Getenv("OPENAI_API_KEY")
	if key == "" {
		print("OpenAI API key not set")
	}

	model := os.Getenv("OPENAI_MODEL")
	if model == "" {
		print("OpenAI model not set")
	}

	r, _ := services.GetResponse(message, key, model)

	var assistant map[string]interface{}
	err := json.Unmarshal([]byte(r), &assistant)
	if err != nil {
		println("Error parsing assistant response", err)
	}

	// TODO (robertomorti) move to mapper
	content, ok := assistant["content"].(string)
	if !ok {
		fmt.Println("Content not found in response")
	}
	e, ok := assistant["event"].(string)
	if !ok {
		fmt.Println("Event not found in response")
	}
	eventType := constants.EventType(e)

	c.workflow.RegisterChatUser(userId.String(), "user_message", message)

	switch eventType {
	case constants.ProductRecommended:
		c.workflow.SendProductRecommendation(userId.String())
	case constants.ProductAsked:
		c.workflow.StartReviewProcess(userId.String())
	case constants.ProductOrder, constants.ProductOrdered:
		// TODO move to mapper
		productName, ok := assistant["product_name"].(string)
		if !ok {
			fmt.Println("Product name not found in response")
		} else {
			order := c.workflow.CreateOrder(userId.String(), productName)
			if order > 0 {
				content = fmt.Sprintf("%s Order number: %d", content, order)
			} else {
				return fmt.Sprintf("Sorry, the product %s is out of our stock. Can we assist you with another request?", productName)
			}
		}

	case constants.DeliveryConfirmed:
		c.workflow.ProcessOrderDelived(userId.String())
	case constants.ReviewGiven:
		productName, _ := assistant["product_name"].(string)
		c.workflow.CompleteReviewProcess(userId.String(), productName, message)
	}

	c.workflow.RegisterChatUser(userId.String(), "bot_response", content)
	return content
}
