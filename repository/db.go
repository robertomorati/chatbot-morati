package repository

import (
	"encoding/json"
	"fmt"
	"log"
	mock_data "morati-chatbot/mocks"
	"morati-chatbot/model"
	"os"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Config struct {
	Database struct {
		Host   string `json:"host"`
		Port   int    `json:"port"`
		User   string `json:"user"`
		DBName string `json:"dbname"`
	} `json:"database"`
}

var DB *gorm.DB

func InitDB() *gorm.DB {
	config := Config{}

	bytes, err := os.ReadFile("./settings/config.json")
	if err != nil {
		log.Fatalf("Error reading config file: %v", err)
	}

	err = json.Unmarshal(bytes, &config)
	if err != nil {
		log.Fatalf("Error unmarshalling config file: %v", err)
	}

	tempDB, err := gorm.Open(mysql.Open(getDSN(config, false)), &gorm.Config{})
	if err != nil {
		log.Fatalf("Failed to connect to MySQL server: %v", err)
	}

	CreateDB(tempDB, config.Database.DBName)

	CloseDB(tempDB)

	DB, err = gorm.Open(mysql.Open(getDSN(config, true)), &gorm.Config{})
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}

	return DB
}

func CreateConnection() *gorm.DB {
	config := Config{}

	bytes, err := os.ReadFile("./settings/config.json")
	if err != nil {
		log.Fatalf("Error reading config file: %v", err)
	}

	err = json.Unmarshal(bytes, &config)
	if err != nil {
		log.Fatalf("Error unmarshalling config file: %v", err)
	}

	DB, err = gorm.Open(mysql.Open(getDSN(config, true)), &gorm.Config{})
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}

	return DB
}

func getDSN(config Config, includeDBName bool) string {

	password := os.Getenv("DB_PASSWORD")
	if password == "" {
		log.Fatal("DB_PASSWORD environment variable is not set")
	}

	if includeDBName {
		return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
			config.Database.User, password, config.Database.Host, config.Database.Port, config.Database.DBName)
	}
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/?charset=utf8mb4&parseTime=True&loc=Local",
		config.Database.User, password, config.Database.Host, config.Database.Port)
}

func CreateDB(db *gorm.DB, dbName string) {
	err := db.Exec(fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s", dbName)).Error
	if err != nil {
		log.Fatalf("Failed to create database: %v", err)
	}
}

func CloseDB(db *gorm.DB) {
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalf("Failed to get database connection: %v", err)
	}

	err = sqlDB.Close()
	if err != nil {
		log.Fatalf("Failed to close database connection: %v", err)
	}
}

func MigrateDB(create_data bool) {
	// automigrate the tables or migrate them to the new schema
	// this migration do not remove fields from tables
	r := NewRepository()
	err := DB.AutoMigrate(&model.User{}, &model.Product{}, &model.Chat{}, &model.Review{}, &model.Order{})
	if err != nil {
		log.Fatalf("Failed to migrate database: %v", err)
	}

	if create_data {

		mock_data.CreateMockUsers(r.DB)
		if err != nil {
			log.Fatalf("Failed to create mock data: %v", err)
		}

		err = r.CreateProducts()
		if err != nil {
			log.Fatalf("Failed to create products: %v", err)
		}
		r.Clean()
	}
}
