package repository

import (
	"encoding/csv"
	"log"
	"morati-chatbot/model"
	"os"
	"strings"

	"gorm.io/gorm"
)

type Repository struct {
	DB *gorm.DB
}

func NewRepository() *Repository {
	db := CreateConnection()
	return &Repository{DB: db}
}

func (r *Repository) Clean() {
	CloseDB(r.DB)
}

func (r *Repository) CreateProducts() error {

	filePath := "./models/products.csv"
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}

	defer file.Close()

	reader := csv.NewReader(file)
	reader.Comma = ','

	products, err := reader.ReadAll()

	if err != nil {
		return err
	}

	var ps []model.Product

	for _, p := range products {
		product := model.Product{
			Name:     p[0],
			Category: p[1],
		}
		ps = append(ps, product)
	}

	if err := r.DB.Create(&ps).Error; err != nil {
		log.Fatalf("Failed to perform bulk create for products: %v", err)
	}

	return nil
}

func (r *Repository) CreateOrder(UserId string, ProductId int64) (model.Order, error) {

	// TODO move to mapper
	order := model.Order{
		UserID:    UserId,
		ProductID: ProductId,
		Status:    model.OrderPending,
	}

	if err := r.DB.Create(&order).Error; err != nil {
		return order, err
	}

	return order, nil
}

func (r *Repository) GetOrder(orderID int64) (model.Order, error) {
	var order model.Order
	if err := r.DB.First(&order, orderID).Error; err != nil {
		return order, err
	}

	return order, nil
}

func (r *Repository) UpdateOrderStatusToDelivered(orderID int64) error {
	status := model.OrderDelivered
	order, err := r.GetOrder(orderID)
	if err != nil {
		return err
	}

	order.Status = status
	if err := r.DB.Save(&order).Error; err != nil {
		return err
	}

	return nil
}

func (r *Repository) GetProductByName(name string) (model.Product, error) {
	var product model.Product
	name = strings.ToLower(name)
	if err := r.DB.Where("LOWER(name) LIKE ?", "%"+name+"%").First(&product).Error; err != nil {
		return product, err
	}

	return product, nil
}

func (r *Repository) CreateChat(userId string, chatType string, message string) (model.Chat, error) {
	// TODO move to mapper
	chat := model.Chat{
		UserID:  userId,
		Type:    model.ChatType(chatType),
		Message: message,
	}

	if err := r.DB.Create(&chat).Error; err != nil {
		return chat, err
	}

	return chat, nil
}

func (r *Repository) CreateReview(userId string, productName string, rating float64, comment string) (model.Review, error) {

	product, err := r.GetProductByName(productName)
	if err != nil {
		return model.Review{}, err
	}
	// TODO move to mapper
	review := model.Review{
		UserID:    userId,
		ProductID: int64(product.ID),
		Rating:    model.Rating(rating),
		Comment:   comment,
	}

	if err := r.DB.Create(&review).Error; err != nil {
		return review, err
	}

	return review, nil
}
